def cache(foo):
    data = {}

    def wrapper(arg1):
        if not data.get(arg1):
            data[arg1] = foo(arg1)
        return data[arg1]
    
    return wrapper


def register_function(my_functions):
    
    def register_function_decorator(a_function):
        my_functions.append(a_function)
        return a_function

    return register_function_decorator


def observable():
    pass


def log_with():
    pass


def inject():
    pass