def make_wrap(*args):
    if args:
        prefix = args[0]
        if len(args) > 1:
            suffix = args[1]
        else:
            suffix = args[0]

    def wrapper(string):
        return f"{prefix}{string}{suffix}"

    return wrapper


def make_append_only_list():
    current_list = []

    def append(elt):
        return current_list.append(elt)

    def copy():
        return current_list.copy()

    return append, copy


def mymap(listArg, funcArg):
    # take a function and a list and apply the function on all elements of the list
    res = []

    for elt in listArg:
        res.append(funcArg(elt))
    return res
