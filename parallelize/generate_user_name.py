# import requests
from functools import wraps
import time

def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time()
        elapsed = te-ts
        print(f"func {f.__name__} took: {elapsed} sec")
        return result
    return wrap

def make_request(*args):
    time.sleep(1)
    return "hello"

@timing
def make_1000_requests():
    results = []
    for i in range(1000):
        results += make_request(i)
    return results
