from generate_user_name import make_request, make_1000_requests, timing
from concurrent.futures import ThreadPoolExecutor
import asyncio

def make_1000_requests_with_threadpool():
    # make_request(0) 
    with ThreadPoolExecutor(max_workers=10) as executor:
        for i in range(10):
            future = executor.submit(make_request, 0)
            print(future.result())



def make_1000_requests_with_processpool():
    make_request(0) 


def make_1000_requests_with_asyncio():
    # make_request(0) 
    async def run():
        pass


if __name__ == '__main__':
    make_1000_requests_with_threadpool()